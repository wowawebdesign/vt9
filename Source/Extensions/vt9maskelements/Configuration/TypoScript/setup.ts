tt_content.vt9maskelements_vt9accordion = FLUIDTEMPLATE
tt_content.vt9maskelements_vt9accordion {
    layoutRootPaths.0 = EXT:vt9maskelements/Resources/Private/Layouts/
    layoutRootPaths.10 = {$plugin.tx_vt9maskelements.view.layoutRootPath}
    partialRootPaths.0 = EXT:vt9maskelements/Resources/Private/Partials/
    partialRootPaths.10 = {$plugin.tx_vt9maskelements.view.partialRootPath}
    templateRootPaths.0 = EXT:vt9maskelements/Resources/Private/Templates/Content/
    templateRootPaths.10 = {$plugin.tx_vt9maskelements.view.templateRootPath}
    templateName = Vt9accordion
    dataProcessing.10 = TYPO3\CMS\Frontend\DataProcessing\DatabaseQueryProcessor
    dataProcessing.10 {
        if.isTrue.field = tx_vt9maskelements_vt9accordionelement
        table = tx_vt9maskelements_vt9accordionelement
        pidInList.field = pid
        where = parentid=###uid### AND deleted=0 AND hidden=0
        orderBy = sorting
        markers.uid.field = uid
        as = data_tx_vt9maskelements_vt9accordionelement
    }
}
tt_content.vt9maskelements_vt9card = FLUIDTEMPLATE
tt_content.vt9maskelements_vt9card {
    layoutRootPaths.0 = EXT:vt9maskelements/Resources/Private/Layouts/
    layoutRootPaths.10 = {$plugin.tx_vt9maskelements.view.layoutRootPath}
    partialRootPaths.0 = EXT:vt9maskelements/Resources/Private/Partials/
    partialRootPaths.10 = {$plugin.tx_vt9maskelements.view.partialRootPath}
    templateRootPaths.0 = EXT:vt9maskelements/Resources/Private/Templates/Content/
    templateRootPaths.10 = {$plugin.tx_vt9maskelements.view.templateRootPath}
    templateName = Vt9card
}
tt_content.vt9maskelements_vt9slider = FLUIDTEMPLATE
tt_content.vt9maskelements_vt9slider {
    layoutRootPaths.0 = EXT:vt9maskelements/Resources/Private/Layouts/
    layoutRootPaths.10 = {$plugin.tx_vt9maskelements.view.layoutRootPath}
    partialRootPaths.0 = EXT:vt9maskelements/Resources/Private/Partials/
    partialRootPaths.10 = {$plugin.tx_vt9maskelements.view.partialRootPath}
    templateRootPaths.0 = EXT:vt9maskelements/Resources/Private/Templates/Content/
    templateRootPaths.10 = {$plugin.tx_vt9maskelements.view.templateRootPath}
    templateName = Vt9slider
    dataProcessing.10 = TYPO3\CMS\Frontend\DataProcessing\DatabaseQueryProcessor
    dataProcessing.10 {
        if.isTrue.field = tx_vt9maskelements_sliderelement
        table = tx_vt9maskelements_sliderelement
        pidInList.field = pid
        where = parentid=###uid### AND deleted=0 AND hidden=0
        orderBy = sorting
        markers.uid.field = uid
        as = data_tx_vt9maskelements_sliderelement
    }
dataProcessing.10 {
        dataProcessing.10 = TYPO3\CMS\Frontend\DataProcessing\FilesProcessor
    dataProcessing.10 {
        if.isTrue.field = tx_vt9maskelements_sliderimage
        references {
            fieldName = tx_vt9maskelements_sliderimage
            table = tx_vt9maskelements_sliderelement
        }
        as = data_tx_vt9maskelements_sliderimage
    }

}
}
