<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "vt9maskelements".
 *
 * Auto generated 06-01-2019 02:31
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
  'title' => 'vt9maskelements',
  'description' => '',
  'category' => 'fe',
  'author' => '',
  'author_email' => '',
  'author_company' => '',
  'state' => 'stable',
  'version' => '0.1.0',
  'constraints' => 
  array (
    'depends' => 
    array (
      'typo3' => '9.5.0-9.5.99',
    ),
    'conflicts' => 
    array (
    ),
    'suggests' => 
    array (
    ),
  ),
);

